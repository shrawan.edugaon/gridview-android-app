package com.edugaon.gridview

import android.os.Bundle
import android.widget.Adapter
import android.widget.ArrayAdapter
import android.widget.GridView
import android.widget.Spinner

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val nameArray = arrayOf("Guddu" ,"Sudish","Chanchal","Guddu" ,"Sudish","Chanchal","Guddu" ,"Sudish","Chanchal","Guddu" ,"Sudish")
        val nameArrayAdapter = ArrayAdapter(this, R.layout.grid_item_layout,nameArray )
        val nameSpinner = findViewById<GridView>(R.id.name_spinner)
        nameSpinner.adapter = nameArrayAdapter
    }
}